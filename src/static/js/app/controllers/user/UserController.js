'use strict';

(function(angular, window, controllerApp) {
	controllerApp.controller('UserController', function($scope, $sce, $parse, $routeParams, User) {
		$scope.user = User.currentUser();
		$scope.params = $routeParams;

		$scope.user.then(function(data) {
			$scope.user = data;

			if ($parse('user.info.bio')($scope)) {
				$scope.user.info.bio = $sce.trustAsHtml($scope.user.info.bio);
			}
		});
	});
})(angular, window, controllerApp);
