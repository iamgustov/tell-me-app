'use strict';

(function(angular, global) {
	var controllerApp = angular.module('tellMeApp.controllers', []);

	global.controllerApp = controllerApp;
})(angular, window);
