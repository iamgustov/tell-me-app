'use strict';

(function(angular, global) {
	var modelApp = angular.module('tellMeApp.models', []);

	global.modelApp = modelApp;
})(angular, window);
