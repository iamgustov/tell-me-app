'use strict';

(function(angular, window, modelApp) {
	modelApp.factory('User', function($q, $http) {
		var user = {},
			api = {
				currentUser: '/static/data/user.json',
				userList: '/static/data/users.json'
			};

		user.currentUser = function() {
			return this.sendResponse(api.currentUser);
		};

		user.userList = function() {
			return this.sendResponse(api.userList);
		};

		user.sendResponse = function(apiPath) {
			var deferred = $q.defer();

			$http.get(apiPath).success(function (data) {
				deferred.resolve(data);
			}).error(function (data) {
				deferred.resolve({});
			});

			return deferred.promise;
		};

		return user;
	});
})(angular, window, modelApp);
