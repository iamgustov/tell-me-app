'use strict';

(function(angular, global) {
	var tellMeApp = angular.module('tellMeApp', ['tellMeApp.models', 'tellMeApp.controllers', 'ngRoute']);

	// Routes
	tellMeApp.config(function($routeProvider) {
		$routeProvider
			.when('/', {controller: 'UserController', templateUrl: '/static/views/user.html'})
			.when('/people', {controller: 'UserController', templateUrl: '/static/views/people.html'})
			.when('/people/:id', {controller: 'UserController', templateUrl: '/static/views/user.html'})
			.otherwise({redirectTo: '/'})
	});

	global.tellMeApp = tellMeApp;
})(angular, window);
